<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landing');
});

Route::get('/test', function () {
	return view('test');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix'=>'admin', 'middleware'=>['auth','role:admin']], function () {
	Route::resource('registration', 'RegistrationController');
	Route::resource('course', 'CourseController');
	Route::get('payment', 'PaymentController@index')->name('payment.index');
	Route::get('payment/{id}', 'PaymentController@edit')->name('payment.edit');
	Route::patch('payment/{id}', 'PaymentController@update')->name('payment.update');
	Route::delete('payment/{id}', 'PaymentController@destroy')->name('payment.destroy');
});

Route::group(['prefix'=>'member', 'middleware'=>['auth','role:member']], function () {
	Route::get('/registration', function(){
		return view('members.registration');
	});
	Route::get('/registration/view', 'RegistrationController@view')->name('regist.view');
	Route::post('/registration', 'RegistrationController@regist')->name('regist.store');

	Route::get('/payment', function(){
		return view('members.payment');
	});
	Route::get('/payment/view', 'PaymentController@view')->name('payment.view');
	Route::post('/payment', 'PaymentController@store')->name('payment.store');
});
