<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Registration;
use Session;

class RegistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $regs = Registration::with('course')->get();

        return view('registrations.index')->with(compact('regs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('registrations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'fullname' => 'required',
          'birth_date' => 'required|date',
          'address' => 'required',
          'postal_code' => 'required|numeric',
          'telephone' => 'required|numeric',
          'brand_name' => 'required',
          'course_id' => 'required|integer',
          'payment_method' => 'required',
          'photo' => 'image|max:2048',
          'status' => 'required',
        ]);

        $registration = Registration::create($request->except('photo','code','user_id'));

        // isi field photo jika ada photo yang diupload
        if ($request->hasFile('photo')) {
            $uploaded_photo = $request->file('photo');

            // mengambil extension file
            $extension = $uploaded_photo->getClientOriginalExtension();

            // membuat nama file random dengan extension
            $filename = md5(time()) . '.' . $extension;

            // memindahkan file ke folder public/img
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img';
            $uploaded_photo->move($destinationPath, $filename);

            // mengisi field photo di registration dengan filename yang baru dibuat
            $registration->photo = $filename;
        }

        //user_id sementara static
        $registration->user_id = 1;
        
        //generate kode unik
        $code = Registration::generateCode();
        $registration->code = $code;

        // save data
        $registration->save();
        
        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menyimpan $registration->fullname"
        ]);

        return redirect()->route('registration.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reg = Registration::findOrFail($id);

        return view('registrations.edit')->with(compact('reg'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'fullname' => 'required',
          'birth_date' => 'required|date',
          'address' => 'required',
          'postal_code' => 'required|numeric',
          'telephone' => 'required|numeric',
          'brand_name' => 'required',
          'course_id' => 'required|integer',
          'payment_method' => 'required',
          'photo' => 'image|max:2048',
          'status' => 'required',
        ]);

        $registration= Registration::findOrFail($id);
        $registration->update($request->all());

        if ($request->hasFile('photo')) {
         // menambil image yang diupload berikut ekstensinya
         $filename = null;
         $uploaded_photo = $request->file('photo');
         $extension = $uploaded_photo->getClientOriginalExtension();

         // membuat nama file random dengan extension
         $filename = md5(time()) . '.' . $extension;
         $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img';
         
         // memindahkan file ke folder public/img
         $uploaded_photo->move($destinationPath, $filename);
         
         // hapus image lama, jika ada
         if ($registration->photo) {
           $old_photo = $registration->photo;
           $filepath = public_path() . DIRECTORY_SEPARATOR . 'img'
           . DIRECTORY_SEPARATOR . $registration->photo;
           try {
             File::delete($filepath);
           } catch (FileNotFoundException $e) {
             // File sudah dihapus/tidak ada
           }
         }

         // ganti field image dengan image yang baru
         $registration->photo = $filename;
         $registration->save();
       }

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil mengedit data"
        ]);

        return redirect()->route('registration.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $registration = Registration::find($id);
        $photo = $registration->photo;
        if(!$registration->delete()) return redirect()->back();

        // handle hapus buku via ajax
        if ($request->ajax()) return response()->json(['id' => $id]);

        // hapus photo lama, jika ada
        if ($photo) {
            $old_photo = $registration->photo;
            $filepath = public_path() . DIRECTORY_SEPARATOR . 'img'
                . DIRECTORY_SEPARATOR . $registration->photo;

            try {
                File::delete($filepath);
            } catch (FileNotFoundException $e) {
                // File sudah dihapus/tidak ada
            }
        }

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Data berhasil dihapus"
        ]);

        return redirect()->route('registration.index');
    }

    public function regist(Request $request)
    {
        $this->validate($request, [
          'fullname' => 'required',
          'birth_date' => 'required|date',
          'address' => 'required',
          'postal_code' => 'required|numeric',
          'telephone' => 'required|numeric',
          'brand_name' => 'required',
          'course_id' => 'required|integer',
          'payment_method' => 'required',
          'photo' => 'image|max:2048'
        ]);

        $registration = Registration::create($request->except('photo','code','user_id'));

        // isi field photo jika ada photo yang diupload
        if ($request->hasFile('photo')) {
            $uploaded_photo = $request->file('photo');

            // mengambil extension file
            $extension = $uploaded_photo->getClientOriginalExtension();

            // membuat nama file random dengan extension
            $filename = md5(time()) . '.' . $extension;

            // memindahkan file ke folder public/img
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img';
            $uploaded_photo->move($destinationPath, $filename);

            // mengisi field photo di registration dengan filename yang baru dibuat
            $registration->photo = $filename;
        }

        //user_id dari session auth
        $registration->user_id = \Auth::user()->id;
        
        //generate kode unik
        $code = Registration::generateCode();
        $registration->code = $code;

        // save data
        $registration->save();
        
        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menyimpan $registration->fullname"
        ]);

        return redirect()->route('home');
    }

    public function view()
    {
        $user_id = \Auth::user()->id;
        $regs = Registration::where('user_id',$user_id)->get();

        return view('members.registration_view')->with(compact('regs'));
    }
}
