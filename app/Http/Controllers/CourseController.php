<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use Session;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::all();

        return view('courses.index')->with(compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('courses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'code' => 'required',
          'name' => 'required',
          'periode' => 'required',
          'price' => 'required',
        ]);

        $course = Course::create($request->all());
        
        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menyimpan $course->name"
        ]);

        return redirect()->route('course.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $course = Course::findOrFail($id);

        return view('courses.edit')->with(compact('course'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'code' => 'required',
          'name' => 'required',
          'periode' => 'required',
          'price' => 'required',
        ]);

        $course = Course::findOrFail($id)->update($request->all());
        
        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil mengedit data"
        ]);

        return redirect()->route('course.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Course::destroy($id);

        Session::flash("flash_notification", [
          "level"=>"success",
          "message"=>"Data Berhasil Dihapus"
        ]);

        return redirect()->route('course.index');
    }
}
