<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Payment;
use DB;
use Session;

class PaymentController extends Controller
{
    /*method for show data*/
    public function index()
    {
    	$payments = Payment::with('registration')->get();

    	return view('payments.index')->with(compact('payments'));
    }

    public function edit($id)
    {
        $payment = Payment::findOrFail($id);

        return view('payments.edit')->with(compact('payment'));
    }

    public function update(Request $request,$id)
    {
        $this->validate($request, [
          'type' => 'required',
          'bank' => 'required',
          'name' => 'required',
          'status' => 'required',
        ]);

        $payment= Payment::findOrFail($id);
        $payment->update($request->all());

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil mengedit data"
        ]);

        return redirect()->route('payment.index');
    }

    public function destroy(Request $request, $id)
    {
        $payment = Payment::find($id);
        $photo = $payment->photo;
        if(!$payment->delete()) return redirect()->back();

        // handle hapus buku via ajax
        if ($request->ajax()) return response()->json(['id' => $id]);

        // hapus photo lama, jika ada
        if ($photo) {
            $old_photo = $payment->photo;
            $filepath = public_path() . DIRECTORY_SEPARATOR . 'img'
                . DIRECTORY_SEPARATOR . $payment->photo;

            try {
                File::delete($filepath);
            } catch (FileNotFoundException $e) {
                // File sudah dihapus/tidak ada
            }
        }

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Data berhasil dihapus"
        ]);

        return redirect()->route('payment.index');
    }

    public function store(Request $request)
    {
    	$this->validate($request, [
          'type' => 'required',
          'bank' => 'required',
          'name' => 'required',
          'amount' => 'required',
          'photo' => 'image|max:2048',
        ]);

        $user_id = \Auth::user()->id;
        $registration = DB::table('registrations')->select('id')->where('user_id',$user_id)->first();

        $payment = Payment::create($request->except('photo','registration_id'));

        // isi field photo jika ada photo yang diupload
        if ($request->hasFile('photo')) {
            $uploaded_photo = $request->file('photo');

            // mengambil extension file
            $extension = $uploaded_photo->getClientOriginalExtension();

            // membuat nama file random dengan extension
            $filename = md5(time()) . '.' . $extension;

            // memindahkan file ke folder public/img
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img';
            $uploaded_photo->move($destinationPath, $filename);

            // mengisi field photo dengan filename yang baru dibuat
            $payment->photo = $filename;
        }

        //user_id sementara static
        $payment->registration_id = $registration->id;

        // save data
        $payment->save();
        
        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Pembayaran Berhasil, silahkan tunggu konfirmasi dari kami."
        ]);

        return redirect()->route('home');
    }

    public function view()
    {
        $user_id = \Auth::user()->id;
        $registration = DB::table('registrations')->select('id')->where('user_id',$user_id)->first();
        if ($registration != null) {
            $payments = Payment::with('registration')->where('registration_id',$registration->id)->get();
        } else {
            $payments = Payment::where('registration_id',0)->get();
        }

        return view('members.payment_view')->with(compact('payments'));
    }
}
