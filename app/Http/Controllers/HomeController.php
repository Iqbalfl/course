<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laratrust\LaratrustFacade as Laratrust;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Laratrust::hasRole('admin')) return $this->adminDashboard();
        if (Laratrust::hasRole('member')) return $this->memberDashboard();

        return view('home');
    }

    protected function adminDashboard()
    {
        return view('home');
    }

    protected function memberDashboard()
    {
        $user_id = \Auth::user()->id;

        $check_regist = DB::table('registrations')->select('id','user_id')->where('user_id',$user_id)->first();
        if ($check_regist != null) {
            $check_payment = DB::table('payments')->select('id','name')->where('registration_id',$check_regist->id)->first();
        } else {
            $check_payment = null;
        }

        return view('members.dashboard')->with(compact('check_regist','check_payment'));
    }
}
