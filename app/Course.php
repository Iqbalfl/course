<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = ['code','name','periode','price'];

    public function registrations()
    {
        return $this->hasMany('App\Registration');
    }
}
