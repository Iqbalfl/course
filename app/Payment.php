<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = ['registration_id','type','bank','name','amount','status'];

    public function registration()
    {
        return $this->belongsTo('App\Registration');
    }

    public static function statusList()
    {
        return [
            'pending' => 'Menunggu Konfirmasi',
            'verified' => 'Pembayaran Diterima'
        ];
    }

    public function getHumanStatusAttribute()
    {
        return static::statusList()[$this->status];
    }
}
