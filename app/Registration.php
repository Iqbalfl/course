<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registration extends Model
{
    protected $fillable = ['user_id','code','fullname','birth_date','address','postal_code','telephone','brand_name','course_id','payment_method','status'];

    //auto kode unik
	public function scopeGenerateCode($query)
	{
	 $unique = false;
	 while ($unique == false) {
	    $code = base_convert(microtime(), 10, 36);
	    $randomID = strtoupper(substr(uniqid($code), 0, 6));
	    $check = $query->where('code',$randomID)->count();
	    if ($check > 0) {
	      $unique = false;
	    } else {
	      $unique = true;
	    }
	 }
	 return $randomID;
	}

	public function course()
    {
        return $this->belongsTo('App\Course');
    }

    public static function statusList()
    {
        return [
            'pending' => 'Menunggu Konfirmasi',
            'verified' => 'Telah Diverifikasi',
            'canceled' => 'Dibatalkan'
        ];
    }

    public function getHumanStatusAttribute()
    {
        return static::statusList()[$this->status];
    }
}
