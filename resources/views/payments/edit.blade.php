@extends('layouts.main')

@section('content')
 <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Pembayaran</h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><i class="fa fa-dashboard"></i> Pembayaran</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Edit Pembayaran</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <form class="form-horizontal" action="{{route('payment.update',$payment->id)}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PATCH">

              <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                <label for="category_id" class="col-md-4 col-sm-4 col-xs-12 control-label">Jenis Pembayaran</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <select class="form-control" name="type">
                        <option value="{{ $payment->type }}">{{ $payment->type }}</option>
                        <option value="">--Pilih Jenis Pembayaran--</option>
                        <option value="administrasi">Pembayaran Administrasi</option>
                    </select>
                    @if ($errors->has('type'))
                        <span class="help-block">
                            <strong>{{ $errors->first('type') }}</strong>
                        </span>
                    @endif
                </div>
              </div>

              <div class="form-group{{ $errors->has('bank') ? ' has-error' : '' }}">
                <label for="category_id" class="col-md-4 col-sm-4 col-xs-12 control-label">Metode Pembayaran</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <select class="form-control" name="bank">
                        <option value="{{ $payment->bank }}">{{ $payment->bank }}</option>
                        <option value="">--Pilih Metode Pembayaran--</option>
                        <option value="transfer-bca">Transfer BCA</option>
                    </select>
                    @if ($errors->has('bank'))
                        <span class="help-block">
                            <strong>{{ $errors->first('bank') }}</strong>
                        </span>
                    @endif
                </div>
              </div>

              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                  <label for="content" class="col-md-4 col-sm-4 col-xs-12 control-label">Atas Nama</label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                      <input type="text" class="form-control" name="name" value="{{ $payment->name }}">
                      @if ($errors->has('name'))
                          <span class="help-block">
                              <strong>{{ $errors->first('name') }}</strong>
                          </span>
                      @endif
                  </div>
              </div>

              <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                  <label for="content" class="col-md-4 col-sm-4 col-xs-12 control-label">Jumlah Bayar</label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                      <input type="number" class="form-control" name="amount" value="{{ $payment->amount }}">
                      @if ($errors->has('amount'))
                          <span class="help-block">
                              <strong>{{ $errors->first('amount') }}</strong>
                          </span>
                      @endif
                  </div>
              </div>

              <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
                  <label for="content" class="col-md-4 col-sm-4 col-xs-12 control-label">Bukti Pembayaran</label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                      <input type="hidden" name="photo" value="{{ $payment->photo }}">
                      @if ($errors->has('photo'))
                          <span class="help-block">
                              <strong>{{ $errors->first('photo') }}</strong>
                          </span>
                      @endif
                      <div class="thumbnail">
                        <img src="{{ url('img',$payment->photo) }}" class="img-rounded">
                      </div>
                  </div>
              </div>

              <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
              <label for="category_id" class="col-md-4 col-sm-4 col-xs-12 control-label">Status</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <select class="form-control" name="status">
                      <option value="{{ $payment->status }}">{{ $payment->human_status }}</option>
                      <option value="">--Pilih Status--</option>
                      <option value="pending">Menunggu Konfirmasi</option>
                      <option value="verified">Telah Diverifikasi</option>
                  </select>
                  @if ($errors->has('status'))
                      <span class="help-block">
                          <strong>{{ $errors->first('status') }}</strong>
                      </span>
                  @endif
              </div>
            </div>
              
              <div class="form-group">
                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
                      <button type="submit" class="btn btn-primary">
                          Simpan
                      </button>
                  </div>
              </div>
            </form>
          </div>
          <!-- /.box-body -->
        </div>
      <!-- /.box -->
    </section>
  <!-- /.content -->
@endsection
