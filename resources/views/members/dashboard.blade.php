@extends('layouts.main')

@section('content')
 <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Dashboard</h1>
      <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @include('layouts._flash')
      <div class="row">
        <div class="col-md-8">    
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Home</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                  <i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              Selamat datang <i>{{ Auth::user()->name }}</i>.
            </div>
            <!-- /.box-body -->
          </div>
         <!-- /.box -->
        </div>

        <div class="col-md-4">    
          <!-- Default box -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Information</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                  <i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              @if ($check_regist == null)
                <!-- small box -->
                <div class="small-box bg-yellow">
                  <div class="inner">
                    <h3>Registrasi</h3>
                    <p>Isi formulir disini!</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-person-add"></i>
                  </div>
                  <a href="{{ url('/member/registration') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
              @else
                <!-- small box -->
                <div class="small-box bg-aqua">
                  <div class="inner">
                    <h3>Registrasi</h3>
                    <p>Cek statusmu disini!</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-person-add"></i>
                  </div>
                  <a href="{{ url('/member/registration/view') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
              @endif
              
              @if ($check_regist != null && $check_payment == null)
                <!-- small box -->
                <div class="small-box bg-green">
                  <div class="inner">
                    <h3>Pembayaran</h3>
                    <p>Isi pembayaran disini!</p>
                  </div>
                  <div class="icon">
                    <i class="fa fa-dollar"></i>
                  </div>
                  <a href="{{ url('/member/payment') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
              @elseif ($check_payment != null)
                <!-- small box -->
                <div class="small-box bg-yellow">
                  <div class="inner">
                    <h3>Pembayaran</h3>
                    <p>Cek pembayaranmu disini!</p>
                  </div>
                  <div class="icon">
                    <i class="fa fa-dollar"></i>
                  </div>
                  <a href="{{ url('/member/payment/view') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
              @endif

            </div>
            <!-- /.box-body -->
          </div>
         <!-- /.box -->
        </div>

      </div>
    </section>
  <!-- /.content -->
@endsection
