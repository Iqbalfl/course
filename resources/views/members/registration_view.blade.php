@extends('layouts.main')

@section('content')
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Registrasi</h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"></i> Registrasi</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    @include('layouts._flash')
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Info Registrasi</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Kode Registrasi</th>
                    <th>Nama Lengkap</th>
                    <th>Tanggal Lahir</th>
                    <th>Alamat</th>
                    <th>Kode Pos</th>
                    <th>No.HP</th>
                    <th>Nama Brand</th>
                    <th>Program Pilihan</th>
                    <th>Metode Pembayaran</th>
                    <th>Foto</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($regs as $key => $item)
                  <tr>
                    <td>{{ ++$key }}</td>
                    <td>{{ $item->code }}</td>
                    <td>{{ $item->fullname }}</td>
                    <td>{{ $item->birth_date }}</td>
                    <td>{{ $item->address }}</td>
                    <td>{{ $item->postal_code }}</td>
                    <td>{{ $item->telephone }}</td>
                    <td>{{ $item->brand_name }}</td>
                    <td>{{ $item->course->name }}</td>
                    <td>{{ $item->payment_method }}</td>
                    <td> <img src="{{ url('img',$item->photo) }}" style="width: 100px;" class="img-rounded" alt="image"> </td>
                    <td>{{ $item->human_status }}</td>
                  </tr>  
                  @endforeach
                </tbody>
              </table>
            </div> 
        </div>
        <!-- /.box-body -->
      </div>
    <!-- /.box -->
    </section>
  <!-- /.content -->
@endsection
