@extends('layouts.main')

@section('content')
 <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Registrasi</h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><i class="fa fa-dashboard"></i> Registrasi</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Formulir Registrasi Peserta</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <form class="form-horizontal" action="{{route('regist.store')}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('fullname') ? ' has-error' : '' }}">
                <label for="content" class="col-md-4 col-sm-4 col-xs-12 control-label">Nama Lengkap</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" class="form-control" name="fullname" value="{{ old('fullname') }}">
                    @if ($errors->has('fullname'))
                        <span class="help-block">
                            <strong>{{ $errors->first('fullname') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('birth_date') ? ' has-error' : '' }}">
                <label for="content" class="col-md-4 col-sm-4 col-xs-12 control-label">Tanggal Lahir</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="date" class="form-control" name="birth_date" value="{{ old('birth_date') }}">
                    @if ($errors->has('birth_date'))
                        <span class="help-block">
                            <strong>{{ $errors->first('birth_date') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                <label for="content" class="col-md-4 col-sm-4 col-xs-12 control-label">Alamat</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <textarea class="form-control" name="address">{{ old('address') }}</textarea>
                    @if ($errors->has('address'))
                        <span class="help-block">
                            <strong>{{ $errors->first('address') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('postal_code') ? ' has-error' : '' }}">
                <label for="content" class="col-md-4 col-sm-4 col-xs-12 control-label">Kode Pos</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="number" class="form-control" name="postal_code" value="{{ old('postal_code') }}">
                    @if ($errors->has('postal_code'))
                        <span class="help-block">
                            <strong>{{ $errors->first('postal_code') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('telephone') ? ' has-error' : '' }}">
                <label for="content" class="col-md-4 col-sm-4 col-xs-12 control-label">Nomor Hp</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="number" class="form-control" name="telephone" value="{{ old('telephone') }}">
                    @if ($errors->has('telephone'))
                        <span class="help-block">
                            <strong>{{ $errors->first('telephone') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('brand_name') ? ' has-error' : '' }}">
                <label for="content" class="col-md-4 col-sm-4 col-xs-12 control-label">Nama Brand</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" class="form-control" name="brand_name" value="{{ old('brand_name') }}">
                    @if ($errors->has('brand_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('brand_name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('course_id') ? ' has-error' : '' }}">
              <label for="category_id" class="col-md-4 col-sm-4 col-xs-12 control-label">Program Kursus</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <select class="form-control" name="course_id">
                      <option value="">--Pilih Program Kursus--</option>
                      @foreach (\App\Course::all() as $course)
                          <option value="{{$course->id}}">{{$course->name}}</option>
                      @endforeach
                  </select>
                  @if ($errors->has('course_id'))
                      <span class="help-block">
                          <strong>{{ $errors->first('course_id') }}</strong>
                      </span>
                  @endif
              </div>
            </div>

            <div class="form-group{{ $errors->has('payment_method') ? ' has-error' : '' }}">
              <label for="category_id" class="col-md-4 col-sm-4 col-xs-12 control-label">Metode Pembayaran</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <select class="form-control" name="payment_method">
                      <option value="">--Pilih Metode Pembayaran--</option>
                      <option value="transfer bca">Transfer BCA</option>
                  </select>
                  @if ($errors->has('payment_method'))
                      <span class="help-block">
                          <strong>{{ $errors->first('payment_method') }}</strong>
                      </span>
                  @endif
              </div>
            </div>

            <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
                <label for="content" class="col-md-4 col-sm-4 col-xs-12 control-label">Foto</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="file" name="photo">
                    @if ($errors->has('photo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('photo') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        Simpan
                    </button>
                </div>
            </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
    <!-- /.box -->
    </section>
  <!-- /.content -->
@endsection
