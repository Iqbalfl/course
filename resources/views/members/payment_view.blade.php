@extends('layouts.main')

@section('content')
 <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Pembayaran</h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><i class="fa fa-dashboard"></i> Pembayaran</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Informasi Pembayaran</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Kode Registrasi</th>
                    <th>Pembayaran</th>
                    <th>Bank</th>
                    <th>Jumlah Bayar</th>
                    <th>Bukti Transfer</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($payments as $key => $item)
                  <tr>
                    <td>{{ ++$key }}</td>
                    <td>{{ $item->registration->code }}</td>
                    <td>{{ $item->type }}</td>
                    <td>{{ $item->bank }}</td>
                    <td>{{ $item->amount }}</td>
                    <td> <img src="{{ url('img',$item->photo) }}" style="width: 100px;" class="img-rounded" alt="image"> </td>
                    <td>{{ $item->human_status }}</td>
                  </tr>  
                  @endforeach
                </tbody>
              </table>
            </div>
        </div>
        <!-- /.box-body -->
      </div>
    <!-- /.box -->
    </section>
  <!-- /.content -->
@endsection
