@extends('layouts.main')

@section('content')
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Kursus</h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('course.index') }}"></i> Kursus</a></li>
        <li class="active"></i>Edit Kursus</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Edit Program Kursus</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <form class="form-horizontal" action="{{route('course.update',$course->id)}}" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">
            <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
                <label for="title" class="col-md-4 col-sm-4 col-xs-12 control-label">Kode Program</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" class="form-control" name="code" value="{{ $course->code }}">

                    @if ($errors->has('code'))
                        <span class="help-block">
                            <strong>{{ $errors->first('code') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="content" class="col-md-4 col-sm-4 col-xs-12 control-label">Nama Program</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" class="form-control" name="name" value="{{ $course->name }}">
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('periode') ? ' has-error' : '' }}">
                <label for="content" class="col-md-4 col-sm-4 col-xs-12 control-label">Periode Program</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" class="form-control" name="periode" value="{{ $course->periode }}">
                    @if ($errors->has('periode'))
                        <span class="help-block">
                            <strong>{{ $errors->first('periode') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                <label for="content" class="col-md-4 col-sm-4 col-xs-12 control-label">Harga Program</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="number" class="form-control" name="price" value="{{ $course->price }}">
                    @if ($errors->has('price'))
                        <span class="help-block">
                            <strong>{{ $errors->first('price') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        Simpan
                    </button>
                </div>
            </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
    <!-- /.box -->
    </section>
  <!-- /.content -->
@endsection
